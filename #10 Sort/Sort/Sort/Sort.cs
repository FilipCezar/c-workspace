﻿using System;
using System.IO;


namespace Sort
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = File.ReadAllLines(@"C:\Users\FI073492\Desktop\Utils\C#-Repo-BitBucket\#10 Sort\Sort\Sort\input.txt");

            int i_index = 0;
            string[] string1 = input[0].Split();
            string[] string2 = input[1].Split();

            int[] vector1 = new int[string1.Length];
            foreach (string nr in string1)
            {
                vector1[i_index] = Int32.Parse(nr);
                ++i_index;
            }

            i_index = 0;
            int[] vector2 = new int[string2.Length];
            foreach (string nr in string2)
            {
                vector2[i_index] = Int32.Parse(nr);
                ++i_index;
            }

            i_index = 0;
            int j_index = 0, k_index = 0;
            int[] res = new int[string1.Length + string2.Length];
            while (i_index < string1.Length && j_index < string2.Length)
            {
                if (vector1[i_index] <= vector2[j_index])
                {
                    res[k_index++] = vector1[i_index++];
                }
                else
                {
                    res[k_index++] = vector2[j_index++];
                }
            }
            while (i_index < string1.Length)
            {
                res[++k_index] = vector1[i_index++];
            }
            while (j_index < string2.Length)
            {
                res[k_index++] = vector2[j_index++];
            }

            foreach (int nr in res)
            {
                Console.Write(nr + " ");
            }
        }
    }
}
