﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Products.Models
{
    public class ProductRepository
    {
        readonly List<Product> _products = new List<Product>
        {
            new Product
            {
                Id=1,
                Name="Product_1",
                Description="Cu mere",
            },
                new Product
            {
                Id=2,
                Name="Product_2",
                Description="Cu visine",
            },
        };

        public List<Product> Products => _products;

        public Product GetById(int id)
        {
            return _products.SingleOrDefault(x => x.Id == id);
        }
    }
}
