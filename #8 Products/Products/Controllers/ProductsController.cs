﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Products.Models;

namespace Products.Controllers
{
    [Route("/[controller]")]

    public class ProductsController : Controller
    {
        readonly ProductRepository _repo;


        public ProductsController(ProductRepository repo)
        {
            _repo = repo;

        }
     
        public IActionResult ReturnProduct(int id)
        {
            var product = _repo.GetById(id);

            return View(product);
        }

        /*public IActionResult Index()
        {
            var products = _repo.Products;

            return View(products);
        }*/
    }
}