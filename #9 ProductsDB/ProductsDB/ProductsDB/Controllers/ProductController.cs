﻿using Microsoft.AspNetCore.Mvc;
using ProductsDB.Models;

namespace ProductsDB.Controllers
{
    [Route("/[controller]")]
    public class ProductController : Controller
    {
        private readonly ProductRepository _repo;

        public ProductController(ProductRepository repo)
        {
            _repo = repo;
        }
        /*public IActionResult Index()
        {
            var products = _repo.Products;

            return View(products);
        }*/
        public IActionResult ReturnProduct(int id)
        {
            var product = _repo.GetById(id);

            return View(product);
        }
    }
}
