﻿using System.Collections.Generic;
using ProductsDB.Models;

namespace ProductsDB.Repositories
{
    public interface IProductRepository
    {
        IEnumerable<Product> Products { get; }

    }
}
