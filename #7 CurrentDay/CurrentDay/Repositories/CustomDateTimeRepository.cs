﻿using CurrentDay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrentDay.Repositories
{
    public class CustomDateTimeRepository
    {
        readonly CustomDateTime _dateTime = new CustomDateTime
        {
            Current = System.DateTime.Today,
        };

        public CustomDateTime CurrentDateTime => DateTime;

        public CustomDateTime DateTime => _dateTime;
    }
}
