﻿using System;
using System.IO;
using System.Linq;

namespace ConvertApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string myFile = @"C:/Users/FI073492/Desktop/Utils/C#/Workspace/#5 Convert/ConvertApp/ConvertApp/File.txt";

            string[] content = File.ReadAllLines(myFile);
            var input = content.SelectMany(line => line.Split(' '));

            string output = string.Join(", ", content);
            Console.WriteLine(output);
        }
    }
}
