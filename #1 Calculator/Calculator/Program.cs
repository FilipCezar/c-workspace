﻿using System;

namespace Calculator
{
    class Program
    {
        public static T Add<T>(T first, T second)
        {

            dynamic nr1 = first;
            dynamic nr2 = second;
            return nr1 + nr2;
        }

        public static T Substract<T>(T first, T second)
        {

            dynamic nr1 = first;
            dynamic nr2 = second;
            return nr1 - nr2;
        }

        public static T Multiply<T>(T first, T second)
        {

            dynamic nr1 = first;
            dynamic nr2 = second;
            return nr1 * nr2;
        }

        public static T Divide<T>(T first, T second)
        {

            dynamic nr1 = first;
            dynamic nr2 = second;
            return nr1 / nr2;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Calculator application");
            Console.WriteLine("----------------------");

            Console.Write("Insert the first number: ");
            var number1 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Insert the second number: ");
            var number2 = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Choose an operation: ");
            Console.WriteLine("1. Press a for ADD operation");
            Console.WriteLine("2. Press s for SUBSTRACT operation");
            Console.WriteLine("3. Press m for MULTIPLY operation");
            Console.WriteLine("4. Press d for DIVIDE operation");

            switch (Console.ReadLine())
            {
                case "a":
                    Console.WriteLine("Your result is: " + Add(number1, number2));

                    break;

                case "s":
                    Console.WriteLine("Your result is: " + Substract(number1, number2));
                    break;

                case "m":
                    Console.WriteLine("Your result is: " + Multiply(number1, number2));
                    break;

                case "d":
                    Console.WriteLine("Your result is: " + Divide(number1, number2));
                    break;

            }

        }
    }
}
