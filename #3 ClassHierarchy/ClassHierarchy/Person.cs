﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassHierarchy
{
    public class Person
    {
        public Person(String name, String surname, String adress)
        {
            this.Name = name;
            this.Surname = surname;
            this.Adress = adress;
        }
        public String Name { get; set; }
        public String Surname { get; set; }
        public String Adress { get; set; }

        public override string ToString() => $"Person info: {Name} {Surname} {Adress}";

    }
}
