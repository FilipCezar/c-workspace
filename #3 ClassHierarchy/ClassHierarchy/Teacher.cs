﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassHierarchy
{
    public class Teacher : Person
    {

        public Teacher(String name, String surname, String adress, TeacherRanks teacherRank) : base(name, surname, adress)
        {
            this.teacherRank = teacherRank;
        }
        
       

        TeacherRanks teacherRank { get; set; }
    }
}
