﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassHierarchy
{
    public class Student : Person
    {
        public Student(String name, String surname, String adress, String id, double average) : base(name, surname, adress)
        {
            this.ID = id;
            this.Average = average;
        }

        String ID { get; set; }
        double Average { get; set; }
    }
}
