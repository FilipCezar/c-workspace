﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassHierarchy
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> personList = new List<Person>();
            List<Person> sortedName = new List<Person>();
            List<Person> sortedSurname = new List<Person>();

            Person p1 = new Person("Filip", "Cezar", "Adress1");
            Person p2 = new Person("Tazlaoanu", "Sandra", "Adress2");
            Person p3 = new Person("Dordea", "Dragos", "Adress3");
            Person p4 = new Person("Voinea", "Tudor", "Adress4");
            Person p5 = new Person("Cretu", "Radu", "Adress5");
            Person s1 = new Student("Lung", "Alexandru", "Adress6", "1", 9.1);
            Person s2 = new Student("Stanciu", "Vlad", "Adress7", "2", 7.8);

            personList.Add(p1);
            personList.Add(p2);
            personList.Add(p3);
            personList.Add(p4);
            personList.Add(p5);
            personList.Add(s1);
            personList.Add(s2);

            sortedName = personList.OrderBy(x => x.Name).ToList();
            sortedSurname = personList.OrderBy(x => x.Surname).ToList();

            Console.WriteLine("The alphabetically sorted list after name: ");
            sortedName.ForEach(Console.WriteLine);


            Console.WriteLine();
            Console.WriteLine("The alphabetically sorted list after surname: ");
            sortedSurname.ForEach(Console.WriteLine);

        }
    }
}
