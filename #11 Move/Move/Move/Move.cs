﻿using System;
using System.IO;

namespace Move
{
    class Move
    {
        static void Main(string[] args)
        {
            var input = File.ReadAllText(@"C:\Users\FI073492\Desktop\Utils\C#-Repo-BitBucket\#11 Move\Move\Move\Input.txt");
            string[] inputStringVect = input.Split(',');
            int[] vector = new int[inputStringVect.Length];
            int i_index = 0, counter = 0;
            foreach (string nr in inputStringVect)
            {
                vector[i_index++] = Int32.Parse(nr);
            }
            for (i_index = 0; i_index < vector.Length; i_index++)
            {
                if (vector[i_index] != 0)
                {
                    vector[counter++] = vector[i_index];
                }
            }
            while (counter < vector.Length)
            {
                vector[counter++] = 0;
            }

            foreach (int nr in vector)
            {
                Console.Write(nr + " ");

            }
        }
    }
}
